var dotenv = require('dotenv')
var dotenvExpand = require('dotenv-expand')
var myEnv = dotenv.config()
dotenvExpand(myEnv)
require('node-ssh')
const { Client, Intents } = require('discord.js');
const { NodeSSH } = require('node-ssh')
const path = require('path')
const { SlashCommandBuilder } = require('@discordjs/builders')
/** 
const bot = new Client({ intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.DIRECT_MESSAGES,
    Intents.FLAGS.GUILD_MEMBERS,
    Intents.FLAGS.DIRECT_MESSAGE_TYPING,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_MESSAGE_TYPING
] })**/
const bot = new Client({ intents: ["GUILDS", "GUILD_MEMBERS", "GUILD_MESSAGES", "DIRECT_MESSAGES", "DIRECT_MESSAGE_TYPING", "DIRECT_MESSAGE_REACTIONS"], partials: ["CHANNEL"] });
const TOKEN = process.env.TOKEN
const PPK = process.env.PPKFILE
const GUILD = process.env.GUILD

const templates = require(path.resolve(__dirname, 'template.json'))
const config = require(path.resolve(__dirname, 'config.json'))
const instances = buildConfig(templates, require(path.resolve(__dirname, 'instances.json')));

const USER = 0
const MOD = 1
const ADMIN = 2

function setupCommands(instances) {
    listcmds = [new SlashCommandBuilder().setName('help').setDescription('Liste des serveurs (commandes)').toJSON()]
    for (instance in instances) {
        listcmds.push(new SlashCommandBuilder().setName(instance).setDescription(instances[instance].desc).toJSON())
    }
    return listcmds
}

function getGuild() {
    return bot.guilds.fetch(GUILD).then(function (guild) {
        return guild
    }, function (error) {
        reject(error)
    })
}

function getMember(guild, author) {
    return guild.members.fetch(author.id)
}

function findRoles(member, roles) {
    //member.roles.fetch()
    return member.roles.cache.find(function (role) {
        /**console.debug(role.name);
        console.debug(roles.indexOf(role.name) >= 0)**/
        return roles.indexOf(role.name) >= 0;
    })
}

function isOwner(member, roles) {

}

function hasRoles(guild, author, roles) {
    return getMember(guild, author).then(function (member) {
        let froles = findRoles(member, roles)
        /**console.debug("ROLES : "+froles)
        console.debug(froles !== undefined)*/
        return froles !== undefined
    }, function (member) { console.debug("ELSE ROLES : " + member); return false; }).catch(function (error) { console.debug("ERROR : " + error); return false; })
}

function hasA2F(authorid) {
    bot.users.fetch(authorid).then(function (user) {
        console.log(JSON.stringify(user))
        return user.mfaEnabled;
    })
}

function checkPerms(author, roles) {
    return getGuild().then(function (guild) {
        let hasroles = hasRoles(guild, author, roles)
        return hasroles
    })
    /**.then(function(callback){
        if(!callback){
            return false
        }
        return true
    })
},function(){return false}).catch(function (){return false})**/
}

function filter(author, instance, message) {
    return checkPerms(author, getLevelRoles(ADMIN, instance)).then(function (callback) {
        if (callback || !message.includes("Distro Details")) {
            return message
        } else {
            split = message.split('\n')
            return split[split.length - 1]
        }
    }, function (error) {
        return "Une erreur est survenue : " + error
    }).catch(function (error) {
        return "Une erreur est survenue : " + error
    })
}

function execSSH(discordcontext, instance, ip, fullpath, args) {
    ssh = new NodeSSH()
    console.log("Connecting to " + ip + " as " + discordcontext.author + " with " + PPK)
    ssh.connect({
        host: ip,
        username: 'bot',
        privateKey: PPK,
        algorithms: {
            ciphers: ["aes256-gcm@openssh.com", "aes128-gcm@openssh.com", "aes256-ctr", "aes192-ctr", "aes128-ctr"],
            kex: ["curve25519-sha256@libssh.org", "diffie-hellman-group-exchange-sha256"],
            hmac: ["hmac-sha2-512-96", "hmac-sha2-256-96", "hmac-sha2-512", "hmac-sha2-256"]
        }
    }).then(function () {
        discordcontext.channel.sendTyping().then(function () {
            console.log("Executing : " + fullpath + " " + args.join(' '))
            ssh.exec(fullpath, args, { cwd: '/home/bot', options: { pty: true }, stream: 'both' }).then(function (chunk) {
                data = chunk.stdout.toString('utf-8').replace(/\x1B\[([0-9]{1,3}([0-9]{1,2})?)?[mGK]/g, '')
                console.log(chunk.stderr.toString('utf-8').replace(/\x1B\[([0-9]{1,3}([0-9]{1,2})?)?[mGK]/g, ''))
                filter(discordcontext.author, instance, data).then(function (message) {
                    if (message.length <= 0) {
                        discordcontext.author.send('Pas de résultat');
                    } else if (message.length >= 2000 * 50) {
                        discordcontext.author.send('Trop de résultats >50 messages');
                    } else {
                        lines = message.split('\n')
                        m = "```diff\n"
                        lines.forEach((line) => {
                            if ((m + '\n' + line + "\n```").length >= 2000) {
                                //console.debug(m + "\n```")
                                discordcontext.author.send(m + "\n```")
                                m = "```\n" + line
                            } else {
                                m += '\n' + line
                            }
                        })
                        //console.debug(m + "\n```")
                        discordcontext.author.send(m + "\n```")
                    }
                    ssh.dispose()
                    discordcontext.react('✅')
                }).catch(function (error) {
                    console.debug("Error on Parsing Output : " + error)
                    discordcontext.author.send("Error : " + error)
                    ssh.dispose()
                    discordcontext.react('❌')
                })
            }, function (error) {
                console.debug("Error on soft command : " + error)
                discordcontext.author.send("Error : " + error)
                ssh.dispose()
                discordcontext.react('❌')
            }).catch(function (error) {
                console.debug("Error on SSH exec command: " + error)
                discordcontext.author.send("Error : " + error)
                ssh.dispose()
                discordcontext.react('❌')
            })
        }).catch(function (error) {
            console.debug("Error on Typing : " + error)
            discordcontext.author.send("Error : " + error)
            discordcontext.react('❌')
        })
        //ssh.withShell(true)

    }, function (error) {
        console.debug("Error on connect : " + error)
        discordcontext.author.send("Error : " + error)
        context.react('❌')
    })
}

function getLevelRoles(level, instance) {
    let roles = []
    if (level <= ADMIN) {
        roles = roles.concat(config["roles"]["admin"])
        if (instance != null) roles.concat(instances[instance].admin)
    }
    if (level <= MOD) {
        roles = roles.concat(config["roles"]["mod"])
    }
    if (level <= USER) {
        roles = roles.concat(config["roles"]["user"])
    }
    return roles
}

function getOptionsWithPerms(author, options, roles) {
    return checkPerms(author, roles).then(function (callback) {
        if (callback) {
            return options
        } else {
            return getNotStaffOptions(options)
        }
    })
}

/**
 * Obtain available options in function of level permissions
 */
function getOptionsByAuthor(author, instance) {
    return getOptionsWithPerms(author, instances[instance].options, getLevelRoles(MOD, instance))
}

function getOptionsHelp(options) {
    listcmd = "```diff\n"
    for (option in options) {
        listcmd += "+ " + option.padEnd(14, " ") + options[option].alt.padEnd(5, " ") + "| " + options[option].desc + "\n"
    }
    return listcmd + "```"
}

function help(context, args) {
    if (args.length > 0) {
        for (key in instances) {
            if (key === args[0]) {
                if (instances[key].enabled)
                    getOptionsByAuthor(context.author, key).then(function (options) {
                        context.author.send("__Usage__ : !" + key + " <COMMAND>\n" + getOptionsHelp(options))
                    })
                else context.author.send("```diff\n-Cette commande est désactivée```")
            }
        }
    } else {
        
        checkPerms(context.author,config["roles"]["admin"]).then((isAdmin)=>{
            message = "__**Commandes**__ : \n```diff\n"
            for (key in instances) {
                if (instances[key].enabled)
                    message += "+ " + key + "\n"
                else if(isAdmin) {
                    message += "- " + key + " (désactivée)\n"
                }
            }
            message += "plus d'informations en spécifiant !help <Command>\n"
            if (isAdmin) message += "\nVous pouvez désactiver une commande en faisant :\n-!disable <commande>\nou réactiver une commande via \n-!enable <commande>"
            message += "```"
            context.author.send(message)
        })
        
    }
}

function special(args, fullpath) {
    if (args[0] == "find" || args[0] == "f") {
        return "find"
    } else if (args[0] == "update-oxide" || args[0] == "uo") {
        return "autoupdate"
    } else if (args[0] == "full-update" || args[0] == "fuu") {
        return "fullupdate"
    } else if (args[0] == "change-seed" || args[0] == "cs") {
        return "wipe"
    } else if (args[0] == "restart-rcon" || args[0] == "rc") {
        return "restartrcon"
    }
    return null
}

function buildConfig(templates, config) {
    builded = {}
    for (instance in config) {
        builded[instance] = {}
        builded[instance] = templates[config[instance]['base']]
        builded[instance] = Object.assign({}, builded[instance], config[instance])
    }
    return builded
}

function isValidOption(key, options) {
    for (option in options) {
        if ((typeof key === 'string' || key instanceof String) && (key === option || options[option].alt === key)) {
            return true;
        }
    }
    return false;
}

function getNotStaffOptions(options) {
    let notStaffOptions = {}
    for (option in options) {
        if (options[option].staff === false) {
            notStaffOptions[option] = options[option]
        }
    }
    return notStaffOptions
}

//console.debug(instances)

bot.login(TOKEN)


bot.on('ready', () => {
    console.info(`Logged in as ${bot.user.tag}!`)
})


bot.on('messageCreate', context => {
    if (context.content.charAt(0) !== "!") return
    content = context.content.substring(1)
    if (context.channel.type.toLowerCase() !== "dm" || content.length == 0) return
    checkPerms(context.author, getLevelRoles(USER, null)).then(function (callback) {
        if (callback) {
            args = content.split(" ")
            if (args.length > 0) {
                command = args.shift()
                if (command === "help") {
                    context.react('✅')
                    help(context, args)
                    return
                }else if (command === "disable" || command === "enable"){
                    checkPerms(context.author,getLevelRoles(ADMIN,args[0])).then((isAdmin)=>{
                        if (isAdmin && args[0] in instances){
                            instances[args[0]].enabled = command === "disable" ? false : true
                            context.react('✅')
                            return
                        } else {
                            context.react('🚫')
                            return 
                        }
                    })
                    
                }
                for (const key in instances) {
                    if (instances[key].enabled && key === command) {
                        //console.debug("KEY: "+key)

                        if (args.length > 0) {
                            //console.debug("KEY: "+key)
                            checkPerms(context.author, getLevelRoles(MOD, key)).then(function (isStaff) {
                                //console.debug("KEY: "+key)
                                if ((isStaff && isValidOption(args[0], instances[key].options)) // if staff check all options
                                    || isValidOption(args[0], getNotStaffOptions(instances[key].options))) { //else user check only user options (and allow them only btw)
                                    //console.debug("KEY: "+key)
                                    context.react('⏲️')
                                    fullpath = instances[key].fullpath
                                    spec = special(args, fullpath)
                                    if (spec != null) {
                                        fullpath = fullpath.split("/")
                                        fullpath.pop()
                                        fullpath.push(spec)
                                        fullpath = fullpath.join("/")
                                        args.shift()
                                    }
                                    if (key === "minecraft") {

                                        if (args[0] == "cfu" || args[0] == "cfupdate")
                                            fullpath = instances[key].fullpath + "/run.sh"
                                        else if (args[0] == "u" || args[0] == "update") {
                                            if (args.length < 2) {
                                                args.push("-h")
                                            }
                                            fullpath = instances[key].fullpath + "/updatePlugins.py"
                                        }
                                        args.shift()
                                    }
                                    //console.debug("KEY: "+key)
                                    //console.debug(instances[key])
                                    //console.debug(instances[key].ip)
                                    execSSH(context, key, instances[key].ip, fullpath, args)
                                }
                                else if (isStaff || !isValidOption(args[0], instances[key].options)){
                                    context.react('❓')
                                    getOptionsByAuthor(context.author, key).then(function (options) {
                                        context.author.send("__Usage__ : !" + key + " <COMMAND>\n" + getOptionsHelp(options))
                                    })
                                }
                                else {
                                    context.react('🚫')
                                }
                            }).catch(function(error){
                                console.debug("Error on Check ADMIN auth : " + error)
                            })

                        } else {
                            context.react('❓')
                            getOptionsByAuthor(context.author, key).then(function (options) {
                                context.author.send("__Usage__ : !" + key + " <COMMAND>\n" + getOptionsHelp(options))
                            })
                        }
                    }
                }
            } else {
                console.log("unkown command")
            }
        }
    }).catch(function(error){
        console.debug("Error on Check user auth : " + error)
    })

})

bot.on("guildMemberUpdate",(oldMember, newMember) =>{
    if (oldMember.roles.cache.size !== newMember.roles.cache.size){
        bot.guilds.cache.delete(newMember.id)
    }
})
