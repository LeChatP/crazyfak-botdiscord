const TOPSERVERTOKEN = process.env.TOPSERVERTOKEN
const CHANNEL = process.env.CHANNEL
const URL = "https://api.top-serveurs.net/v1/votes/last?server_token="+TOPSERVERTOKEN
const voted = [
    "a voté pour le serveur !",
    "nous fais l'honneur d'user de son temps pour voter pour le serveur!",
    "utilise sa 2G pour voter sur le serveur, il faut être courageux",
    "a acheté un PC avec une RTX pour voter sur le serveur, ça fais cher le vote",
    "vole les votes des concurrents mais c'est pour soutenir le serveur alors on ne le bannira pas cette fois",
    "a achèté un vote pour nous soutenir, c'est illégal, nous le bannirons un jour où nous trouverons ça pas très équitable",
    "nous soutient avec son vote pour le serveur!",
    "mérite une place de meilleur joueur pendant 3 secondes avec son vote",
    "ne veut qu'une chose : qu'on le rémunère pour les votes qu'il fait",
    "préfère voter pour le serveur que de voter à la mairie, même si c'est faux ça prouve une certaine preuve de loyauté. On se CALME. C'est une dictature ici, on ne vote pas par choix.",
    "vote pour le serveur dans un pays dictatorial, c'est le comble.",
    "a fais un choix, il vote pour le serveur.",
    "vote pour la communauté qu'il chérit de tout son coeur (c'est à dire nous).",
    "vote. C'est important, mais c'est encore mieux quand on vote pour le serveur",
    "vote pour notre serveur après plusieurs lettres de menaces",
    "vote pour nous car nous l'harcelons depuis des mois mais il nous trouve sympa",
    "vote pour nous malgré qu'il nous aime pas. C'est gentil mais on le prends quand même mal.",
    "vote. Si tu veux un meilleur message, il faut revoter (dsl)",
    "a voté. Pas aux municipales mais sur TopServeur.",
    "n'a pas mis ses lunettes a voté pour nous sans faire exprès.",
    "a réussi a voter avec ses pieds!",
    "a crammé son forfait 5G pour nous voter.",
    "fais du trafic d'armes pour se permettre de nous voter. Nous ne prenons pas part à ces activités.",
    "a mangé toute la tablette de chocolat. Il a aussi voté pour le serveur.",
    "vote les yeux fermés.",
    "est somnanbule et arrive a voter pour nous pendant son sommeil",
    "a trouvé son programme sur son lave vaisselle pour voter.",
    "vote à chaque café qu'il prends. Attention à la cardio",
    "a trouvé le gâteau que GlaDOS lui a promit, il vote pour le serveur avant de le manger",
    "a coché sur sa todolist 'voter pour le serveur'",
    "a trouvé comment voter pour le serveur",
    "a suivi le tuto youtube 'comment utiliser un navigateur internet' pour voter. On progresse tous à notre manière",
    "utilise internet explorer 12 pour voter.",
    "vote. Pas de commentaire",
    "<insérez une phrase rigolo pour indiquer que le joueur a voté>",
    "joue pendant son travail, mais il vote pour nous alors on dira rien",
    "vote mais ne joue pas sur le serveur. C'est son choix.",
    "écoute les actionnaires, et décide de voter pour le serveur.",
    "a réussi son diplôme pour apprendre a voter pour le serveur.",
    "vote avec ses mineurs de cryptomonnaies et cela lui coûte pas très cher.",
    "prépare sa campagne présidentielle, en votant pour nous il espère un geste de notre part.",
    "s'est levé de son lit pour voter.",
    "n'a pas hésité à voter pour nous",
    "vote pour nous. C'est fait!",
    "reviens de loin, mais il a quand même voté pour nous. Lui au moins.",
    "nous a sauvé d'un COVID-21 en votant pour nous",
    "n'a pas gagné au loto, mais a voté pour nous, c'est une mise de valeur sûre au moins.",
    "commit & push des obscénités sur le noyau linux. C'est drôle, mais c'est quand même mieux de voter pour nous.",
    "est VACban, mais vote quand même pour nous",
    "est banni de notre serveur, mais il nous n'en veut pas, il vote quand même pour nous. Une vrai preuve de loyauté. Mais il reste banni",
    "est désormais banni du serveur en votant pour nous. Il n'a aucun regrets et recommencera avec plaisir.",
    "STONKS un MAX en votant pour le serveur",
    "est rassuré en votant pour nous",
    "vote avec son tapis connecté.",
    "Fais une pompe à chaque vote. C'est pas gagné.",
    "n'a pas lu ce message et s'en fiche. (Il vote tout de même pour le serveur)",
    "s'est fait RAID mais ne le sait pas encore. Il votera la prochaine fois pour retrouver son stuff.",
    "a tiré sur un hélicoptère sauf qu'il n'était pas sur Rust. Il vote depuis sa cellule en prison.",
    "n'abandonnera jamais, je dirais même, he never gonna give us up!",
    "ne connait pas Time-Craft mais vote quand même.",
    "joue au Quiddich dans la vrai vie. Mais il vote pour le serveur.",
    "utilise des cheat pour voter pour le serveur",
    "n'a pas internet mais arrive à voter.",
    "n'a pas d'humour mais il vote pour nous donc on rigole quand même.",
    "dit 'pain au chocolat' mais il vote pour nous donc c'est toléré.",
    "dit 'chocolatine' mais il vote pour nous donc c'est toléré.",
    "vote pour nous en mangeant",
    "s'est crée un système d'exploitation juste pour voter pour nous.",
    "a appellé le support microsoft pour savoir comment voter pour nous, il a finalement réussi.",
    "pédale sur son vélo pour avoir l'électricité juste pour voter pour nous.",
    "chante du Jul et vote pour le serveur."
]
const jsoning = require('jsoning')
const db = new jsoning("database.json")
const http = require("http")
var cron = require("cron")
var job = cron.CronJob('* * * * * *',updateTopServeur)
job.start()

function setLastVote(vote){
    await db.set("lastvote",vote.datetime)
}

function isNew(vote){
    return db.get("lastvote") < vote.datetime
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max))
}

function sendToTopServerChannel(vote){
    client.channels.get(CHANNEL).send(vote + " " + voted[getRandomInt(voted.length)])
}

function updateTopServeur(){
    if (await db.has("lastvote")){
        http.request(URL,function(error, response, body){
            if(!error && response.statusCode == 200){$
                json = JSON.parse(body)
                for(vote in json.votes){
                    if (isNew(vote))
                        sendToTopServerChannel(vote)
                }
                setLastVote(vote)
            }
        })
    }
}
